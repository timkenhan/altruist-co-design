CC-BY 4.0
© Timothy Kenno Handojo 2018, Some Rights Reserved
This work is licensed under Creative Commons Attribution 4.0 License
https://creativecommons.org/licenses/by/4.0/


This directory contains the original files of company's document template.


SOURCE

You can find the template on sources/, stored in open document format.
The document was created using LibreOffice 6.


FONTS

All the necessary fonts are included under fonts/ directory.
It is necessary to have Oswald and Roboto fonts.
Ensure they're installed before opening (esp. editing) any source file.
To install them, refer to your system-specific manual.


DOCS

More information on the source files can be found on docs/ directory.
